package waig548.testbot.event;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import waig548.testbot.commandmeta.CommandContext;
import waig548.testbot.commandmeta.CommandContextParser;
import waig548.testbot.commandmeta.CommandManager;
import waig548.testbot.core.Bot;
import waig548.testbot.definitions.LogFormatter;

import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

public class MessageEventHandler extends ListenerAdapter
{
	private final CommandContextParser commandContextParser;
	private final CommandManager commandManager;
	private final Logger log=Logger.getLogger(MessageEventHandler.class.getSimpleName());
	
	public MessageEventHandler(CommandContextParser commandContextParser, CommandManager commandManager)
	{
		this.commandContextParser=commandContextParser;
		this.commandManager=commandManager;
		this.log.setUseParentHandlers(false);
		ConsoleHandler handler=new ConsoleHandler();
		handler.setFormatter(new LogFormatter());
		this.log.addHandler(handler);
	}
	
	@Override
	public void onMessageReceived(MessageReceivedEvent event)
	{
		if(event.getAuthor().isBot()) return;
		System.out.println("Message received from "+event.getAuthor().getName()+": "+event.getMessage().getContentDisplay());
		this.log.info(event.getMessage().getContentDisplay());
		if(!event.getMessage().getContentRaw().matches("^"+Bot.getPrefix(event.getGuild())+".+$")) return;
		CommandContext context=this.commandContextParser.parse(event);
		if(context!=null)
			CommandManager.prefixCalled(context);
		else
			event.getTextChannel().sendMessage("Unknown command.").complete();
		
	}
	
}
