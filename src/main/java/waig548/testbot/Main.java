package waig548.testbot;

import waig548.testbot.commandmeta.CommandInitializer;
import waig548.testbot.core.Bot;

public class Main
{
	
	public static void main(String[] args)
	{
		Bot.init();
		CommandInitializer.init();
	}
	
}
