package waig548.testbot.command.info;

import net.dv8tion.jda.core.EmbedBuilder;
import waig548.testbot.commandmeta.Command;
import waig548.testbot.commandmeta.CommandContext;
import waig548.testbot.commandmeta.CommandRegistry;
import waig548.testbot.core.Bot;
import waig548.testbot.definitions.Module;

import java.awt.Color;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class CommandListCommand extends Command
{
	
	public CommandListCommand(String name, String... aliases)
	{
		super(name, aliases);
		this.help.append("Show all available commands.");
	}
	
	@Override
	public void invoke(CommandContext context)
	{
		String prefix=Bot.getPrefix(context.guild);
		if(context.args.length>0)
		{
			Optional<Module> target=Module.parse(context.args[0]);
			//System.out.println(target.getTranslationKey());
			
			if(!target.isPresent() && !context.args[0].equalsIgnoreCase("all"))
			{
				String sb="No such module. Show a list of available modules with"
						+" `"+Bot.getPrefix(context.guild)+"commands`";
				context.textChannel.sendMessage(sb).complete();
			}
			else
			{
				EmbedBuilder eb=new EmbedBuilder().setColor(Color.CYAN);
				boolean includeAll=context.args[0].equalsIgnoreCase("all");
				
				for(Module m: Module.values())
				{
					if((includeAll && m.isEnabledIn(context.guild)) || (target.isPresent() && target.get()==m))
					{
						String title=m.getAltName();
						StringBuilder[] desc=new StringBuilder[3];
						for(int i=0; i<desc.length; i++)
						{
							desc[i]=new StringBuilder();
						}
						List<Command> commandList=CommandRegistry.getCommandModule(m).getCommands();
						Collections.sort(commandList);
						int i=0;
						for(Command c: commandList)
						{
							desc[i%3].append(prefix).append(c.getName()).append("\n");
							i++;
						}
						eb.addField(title, desc[0].toString(), true);
						eb.addField("", desc[1].toString(), true);
						eb.addField("", desc[2].toString(), true);
					}
				}
				eb.addField("", "Say `"+prefix+"help <command>` to get more information on a specific command.", false);
				context.textChannel.sendMessage(eb.build()).complete();
			}
		}
		else
		{
			StringBuilder sb=new StringBuilder();
			sb.append("Say `").append(prefix).append(this.getSyntax());
			sb.append("` to show commands for a module, or `").append(prefix)
					.append("commands all` to show all commands.\n");
			sb.append("\n");
			sb.append("Enabled modules in this guild: ");
			Iterator<Module> iterator=Arrays.asList(Module.values()).iterator();
			while(iterator.hasNext())
			{
				Module m=iterator.next();
				if(m.isEnabledIn(context.guild))
				{
					sb.append("**").append(m.getAltName()).append("**");
					if(iterator.hasNext()) sb.append(", ");
				}
			}
			context.textChannel.sendMessage(sb.toString()).complete();
		}
	}

	@Override
	public String getSyntax()
	{
		return "commands <module>";
	}
}
