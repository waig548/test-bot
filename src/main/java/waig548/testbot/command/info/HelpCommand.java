package waig548.testbot.command.info;

import net.dv8tion.jda.core.EmbedBuilder;
import waig548.testbot.commandmeta.Command;
import waig548.testbot.commandmeta.CommandContext;
import waig548.testbot.commandmeta.CommandRegistry;
import waig548.testbot.core.Bot;

import java.awt.Color;

public class HelpCommand extends Command
{
	public HelpCommand(String name, String... aliases)
	{
		super(name, aliases);
		this.help.append("Get help about certain command.");
	}
	
	@Override
	public void invoke(CommandContext context)
	{
		if(context.args.length>0)
		{
			Command target=CommandRegistry.findCommand(context.args[0]);
			if(target!=null)
			{
				StringBuilder sb=new StringBuilder();
				
				if(target.getAliases().size()>0)
					sb.append("**Aliases**: ").append(target.getAliases()).append("\n");
				sb.append(target.getHelp());
			
				EmbedBuilder eb=new EmbedBuilder().setColor(Color.CYAN);
				eb.setTitle("Usage: "+Bot.getPrefix(context.guild)+target.getSyntax());
				eb.setDescription(sb.toString());
				
				context.textChannel.sendMessage(eb.build()).complete();
			}
		}
	}
	
	@Override
	public String getSyntax()
	{
		return "help <command>";
	}
	
}
