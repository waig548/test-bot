package waig548.testbot.command.fun;

import waig548.testbot.commandmeta.Command;
import waig548.testbot.commandmeta.CommandContext;
import waig548.testbot.util.math.DiceExpression;

public class RollCommand extends Command
{
	
	
	public RollCommand(String name, String... aliases)
	{
		super(name, aliases);
		this.help.append(
			"Dice expression can contain the standard representations of dice in text form (e.g. 2d20 is two 20-sided dice), with addition and subtraction allowed.");
		this.help.append("If no dice expression was given, rolls a d20.");
	}
	
	private static String postProcess(String expr)
	{
		return postProcess(expr, false);
	}
	
	private static String postProcess(String expr, boolean negative)
	{
		if(expr.contains(".0"))
			return postProcess(expr.replace(".0", ""));
		if(expr.matches("^\\(.+\\)$"))
		{
			if(negative)
				return "-"+expr.substring(1, expr.length()-1).replace("+", "-").replace("=", "=-");
			return expr.substring(1, expr.length()-1);
		}
		int wraps=0, wards=0;
		for(int i=0; i<expr.length(); i++)
		{
			if(expr.charAt(i)=='[') wards++;
			if(expr.charAt(i)==']') wards--;
			if(expr.charAt(i)=='(') wraps++;
			if(expr.charAt(i)==')') wraps--;
			if(wraps==0 && wards==0)
				if(expr.charAt(i)=='+' || expr.charAt(i)=='-')
				{
					String left=expr.substring(0, i);
					if(left.matches("^\\[.+]$"))
						left=left.substring(1, left.length()-1);
					String right=expr.substring(i+1);
					if(right.matches("^\\[.+]$"))
						right=right.substring(1, right.length()-1);
					if(expr.charAt(i)=='+')
						return postProcess(left, negative)+", "+postProcess(right);
					else if(expr.charAt(i)=='-')
						return postProcess(left, negative)+", "+postProcess(right, true);
				}
		}
		if(expr.matches("^\\[.+]$"))
			return postProcess(expr.substring(1, expr.length()-1), negative);
		if(negative)
			return "-"+expr;
		return expr;
	}
	
	@Override
	public void invoke(CommandContext context)
	{
		DiceExpression<Integer> expression;
		Number result;
		if(context.args.length>0)
		{
			StringBuilder expr=new StringBuilder();
			for(String s: context.args)
				expr.append(s);
			expression=DiceExpression.parse(/*this.preProcess(*/expr.toString());
		}
		else expression=DiceExpression.parse("d20");
		result=expression.calculate();
		System.out.println(expression.toString());
		context.textChannel.sendMessage(
				"<@"+context.member.getUser().getIdLong()+"> rolled **"+result.intValue()+"**. "+"("+postProcess(
						expression.toString())+")").complete();
	}
	
	@Override
	public String getSyntax()
	{
		return "roll [dice expression]";
	}
}
