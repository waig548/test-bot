package waig548.testbot.command.config;

import waig548.testbot.commandmeta.Command;
import waig548.testbot.commandmeta.CommandContext;

public class PrefixCommand extends Command//TODO add custom prefix support
{
	
	public PrefixCommand(String name, String... aliases)
	{
		super(name, aliases);
		this.help.append("Configure the prefix for this guild.");
	}
	
	@Override
	public void invoke(CommandContext context)
	{
	
	}
	
	@Override
	public String getSyntax()
	{
		return "prefix <prefix>";
	}
	
}
