package waig548.testbot.command.util;

import waig548.testbot.commandmeta.Command;
import waig548.testbot.commandmeta.CommandContext;
import waig548.testbot.util.math.Expression;
import waig548.testbot.util.math.Operator;

public class CalcCommand extends Command
{
	public CalcCommand(String name, String... aliases)
	{
		super(name, aliases);
		this.help.append("Calculates the result of given math expression.\n");
		this.help.append("Supported operations: ");
		for(Operator operator: Operator.values())
			if(operator.isDependencyMet(null) && operator!=Operator.EMPTY)
			{
				this.help.append(operator.getName()).append("(").append(operator.getSyntax()).append("), ");
			}
		this.help.replace(this.help.lastIndexOf(", "),this.help.length()-1,".\n");
		
	}
	
	@Override
	public void invoke(CommandContext context)
	{
		if(context.args.length==0)
		{
			context.textChannel.sendMessage("Invalid expression.").complete();
			return;
		}
		StringBuilder expr=new StringBuilder();
		for(String s: context.args)
			expr.append(s);
		Expression<Double> expression;
		try
		{
			expression=Expression.parse(expr.toString());
		}catch(NumberFormatException e)
		{
			context.textChannel.sendMessage("Invalid expression.").complete();
			return;
		}
		
		Double result=expression.calculate();
		System.out.println(expression.toString());
		String str=String.valueOf(result);
		if(str.matches("^\\p{javaDigit}+\\.0$"))
			str=str.replace(".0", "");
		context.textChannel.sendMessage(result.isInfinite()? "undefined":str).complete();
	}
	
	@Override
	public String getSyntax()
	{
		return "math <expression>";
	}
}
