package waig548.testbot.command.util;

import waig548.testbot.commandmeta.Command;
import waig548.testbot.commandmeta.CommandContext;

public class NumberFormatConversionCommand extends Command
{
	private static final String NUM_CHAR_REF="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public NumberFormatConversionCommand(String name, String... aliases)
	{
		super(name, aliases);
		this.help.append("Convert the given number(whole number, base must be between 2 and 36) to the specific base.");
	}
	@Override
	public void invoke(CommandContext context)
	{
		int src=10;
		if(context.args.length>2)
			src=Integer.parseInt(context.args[2]);
		long target=Long.parseLong(context.args[1]);
		long num=0;
		String input=context.args[0];
		if(src==16)
			input=input.replace("0x", "");
		try
		{
			num=Long.parseLong(input, (int) src);
		}
		catch(NumberFormatException e)
		{
			context.textChannel.sendMessage("Invalid syntax").complete();
			return;
		}
		
		
		StringBuilder result=new StringBuilder();
		while(num>0)
		{
			result.append(NUM_CHAR_REF.charAt((int)(num%target)));
			num/=target;
		}
		if(target==2)
			if(result.length()%8!=0)
				for(int i=8; i>result.length()%8; i--)
					result.append("0");
		if(target==8)
			result.append("0");
		if(target==16)
			result.append("x0");
		context.textChannel.sendMessage(result.reverse().toString()).complete();
	}
	
	@Override
	public String getSyntax()
	{
		return "convert <number> <targetBase> [srcBase]";
	}
}
