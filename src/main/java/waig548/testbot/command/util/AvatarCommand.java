package waig548.testbot.command.util;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.User;
import waig548.testbot.commandmeta.Command;
import waig548.testbot.commandmeta.CommandContext;
import waig548.testbot.core.Bot;

import java.awt.Color;

public class AvatarCommand extends Command
{
	public AvatarCommand(String name, String... aliases)
	{
		super(name, aliases);
		this.help.append("Retrieve the avatar of yourself or user by the given id.");
	}
	
	@Override
	public void invoke(CommandContext context)
	{
		EmbedBuilder eb=new EmbedBuilder().setColor(Color.CYAN);
		
		long targetID=0;
		if(context.args.length==0)
		{
			targetID=context.member.getUser().getIdLong();
		}
		else
		{
			if(context.args[0].matches("\\p{Digit}")) targetID=Long.parseLong(context.args[0]);
			else
			{
				//TODO full handle support
			}
		}
		User targetUser=Bot.bot.getUserById(targetID);
		String tag="";
		try
		{
			tag=targetUser.getAsTag();
		}
		catch(NullPointerException e)
		{
			System.out.println("Unable to get the tag of user: "+targetID);
		}
		finally
		{
			eb.setDescription("Avatar of "+(tag.equals("")? targetID: tag));
			eb.setImage(targetUser.getAvatarUrl());
			context.textChannel.sendMessage(eb.build()).complete();
		}
	}
	
	@Override
	public String getSyntax()
	{
		return "avatar [user ID, user tag]";
	}
	
}
