package waig548.testbot.core;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Guild;
import waig548.testbot.commandmeta.CommandContextParser;
import waig548.testbot.commandmeta.CommandManager;
import waig548.testbot.event.MessageEventHandler;

import javax.security.auth.login.LoginException;
import java.util.Random;

public class Bot
{
	public static JDA bot;
	public static final Random RANDOM=new Random();
	public String prefix;
	
	public static void init()
	{
		JDABuilder builder=new JDABuilder(AccountType.BOT);
		String token="";
		builder.setToken(token);
		builder.addEventListener(new MessageEventHandler(new CommandContextParser(), new CommandManager()));
		builder.setGame(Game.of(Game.GameType.DEFAULT, "=>help"));
		try
		{
			bot=builder.build();
		}
		catch(LoginException e)
		{
			System.err.println("Invalid token");
		}
	}
	
	private static String getPrefix(long guildID)
	{
		
		return ">";
	}
	
	public static String getPrefix(Guild guild)
	{
		return getPrefix(guild.getIdLong());
	}
}
