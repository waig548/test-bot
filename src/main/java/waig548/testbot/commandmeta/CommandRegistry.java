package waig548.testbot.commandmeta;

import waig548.testbot.definitions.Module;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class CommandRegistry
{
	private static final Map<Module, CommandRegistry> modules=new HashMap<>();
	private final Module module;
	private Map<String, Command> registry=new LinkedHashMap<>();
	
	public CommandRegistry(@Nonnull Module module)
	{
		this.module=module;
		registerModule(this);
	}
	
	private static void registerModule(@Nonnull CommandRegistry registry)
	{
		modules.put(registry.module, registry);
	}
	
	@Nonnull
	public static CommandRegistry getCommandModule(@Nonnull Module module)
	{
		CommandRegistry mod=modules.get(module);
		if(mod==null) throw new IllegalStateException("No such module registered: "+module.name());
		return mod;
	}
	
	@Nullable
	public static Command findCommand(@Nonnull String name)
	{
		return modules.values().stream().map(cr->cr.getCommand(name)).filter(Objects::nonNull).findAny().orElse(null);
	}
	
	public static int getTotalSize()
	{
		return modules.values().stream().mapToInt(CommandRegistry::getSize).sum();
	}
	
	public static Set<String> getAllRegisteredCommandAndAliases()
	{
		return modules.values().stream().flatMap(cr->cr.getRegisteredCommandsAndAliases().stream())
				.collect(Collectors.toSet());
	}
	
	public void registerCommand(@Nonnull Command command)
	{
		String name=command.getName();
		this.registry.put(name, command);
		for(String alias: command.getAliases())
			this.registry.put(alias, command);
		command.setModule(this.module);
	}
	
	public List<Command> getCommands()
	{
		return new ArrayList<>(this.registry.values());
	}
	
	public List<Command> getDeDuplicatedCommands()
	{
		List<Command> result=new ArrayList<>();
		for(Command c: this.registry.values())
			if(!result.contains(c)) result.add(c);
		return result;
	}
	
	@Nonnull
	public Set<String> getRegisteredCommandsAndAliases()
	{
		return this.registry.keySet();
	}
	
	public int getSize()
	{
		return this.registry.size();
	}
	
	@Nullable
	public Command getCommand(@Nonnull String name)
	{
		return this.registry.get(name);
	}
	
}
