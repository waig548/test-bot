package waig548.testbot.commandmeta;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;
import waig548.testbot.util.messages.Context;

public class CommandContext extends Context
{
	public final String[] args;
	public final Command command;
	
	public CommandContext(Guild guild, TextChannel channel, Member member, String[] args, Command command)
	{
		super.guild=guild;
		super.textChannel=channel;
		super.member=member;
		this.args=args;
		this.command=command;
	}
}
