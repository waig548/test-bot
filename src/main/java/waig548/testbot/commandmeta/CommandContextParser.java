package waig548.testbot.commandmeta;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import waig548.testbot.core.Bot;
import waig548.testbot.definitions.LogFormatter;

import java.util.Arrays;
import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

public class CommandContextParser
{
	private static final Logger log=Logger.getLogger(CommandContext.class.getSimpleName());
	
	public CommandContextParser()
	{
		log.setUseParentHandlers(false);
		ConsoleHandler handler=new ConsoleHandler();
		handler.setFormatter(new LogFormatter());
		log.addHandler(handler);
	}
	
	public CommandContext parse(MessageReceivedEvent event)
	{
		String content=event.getMessage().getContentRaw();
		String prefix=Bot.getPrefix(event.getGuild());
		String input="";
		if(content.startsWith(prefix)) input=content.substring(prefix.length());
		if(input.isEmpty()) return null;
		String[] args=input.split("\\p{javaSpaceChar}");
		if(args.length==0) return null;
		Command command=CommandRegistry.findCommand(args[0].toLowerCase());
		if(command==null)
		{
			log.info("Unknown command:\t"+args[0]);
			return null;
		}
		else
		{
			Guild guild=event.getGuild();
			TextChannel channel=event.getTextChannel();
			Member member=event.getMember();
			
			return new CommandContext(guild, channel, member, Arrays.copyOfRange(args, 1, args.length), command);
		}
	}
}
