package waig548.testbot.commandmeta;

import waig548.testbot.command.config.PrefixCommand;
import waig548.testbot.command.fun.RollCommand;
import waig548.testbot.command.info.CommandListCommand;
import waig548.testbot.command.info.HelpCommand;
import waig548.testbot.command.util.AvatarCommand;
import waig548.testbot.command.util.CalcCommand;
import waig548.testbot.command.util.NumberFormatConversionCommand;
import waig548.testbot.definitions.Module;

public class CommandInitializer
{
	public static void init()
	{
		CommandRegistry configModule=new CommandRegistry(Module.CONFIG);
		configModule.registerCommand(new PrefixCommand("prefix"));
		
		CommandRegistry infoModule=new CommandRegistry(Module.INFO);
		infoModule.registerCommand(new HelpCommand("help"));
		infoModule.registerCommand(new CommandListCommand("commands"));
		
		CommandRegistry utilityModule=new CommandRegistry(Module.UTIL);
		utilityModule.registerCommand(new AvatarCommand("avatar"));
		utilityModule.registerCommand(new CalcCommand("calc"));
		utilityModule.registerCommand(new NumberFormatConversionCommand("convert"));
		
		CommandRegistry funModule=new CommandRegistry(Module.FUN);
		funModule.registerCommand(new RollCommand("roll"));
	}
}
