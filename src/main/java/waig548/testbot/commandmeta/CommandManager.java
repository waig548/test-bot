package waig548.testbot.commandmeta;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;

public class CommandManager
{
	public static void prefixCalled(CommandContext context)
	{
		Guild guild=context.guild;
		Command invoked=context.command;
		TextChannel channel=context.textChannel;
		Member invoker=context.member;
		invoked.invoke(context);
	}
}
