package waig548.testbot.commandmeta;

import org.jetbrains.annotations.NotNull;
import waig548.testbot.definitions.Module;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;

public abstract class Command implements Comparable<Command>
{
	protected final StringBuilder help=new StringBuilder();
	private final String name;
	private final List<String> aliases;
	@Nullable
	private Module module;
	
	/**
	 * Creates a command with given name and aliases.
	 *
	 * @param name    the name of the command
	 * @param aliases the aliases of the command
	 */
	protected Command(String name, String... aliases)
	{
		this.name=name;
		this.aliases=Arrays.asList(aliases);
	}
	
	/**
	 * Returns the name of the command.
	 *
	 * @return the name of the command
	 */
	public String getName()
	{
		return this.name;
	}
	
	/**
	 * Returns the list of the aliases of the command.
	 *
	 * @return the aliases of the command as a {@link java.util.List}
	 */
	public List<String> getAliases()
	{
		return this.aliases;
	}
	
	/**
	 * Operations to do when the command is invoked.
	 *
	 * @param context command input
	 */
	public abstract void invoke(CommandContext context);
	
	/**
	 * Returns the help text about the command
	 *
	 * @return the help text about the command
	 */
	public final String getHelp()
	{
		return this.help.toString();
	}
	
	/**
	 * Returns the module the command is assigned to.
	 *
	 * @return the module that the command is assigned to
	 */
	@Nullable
	public Module getModule()
	{
		return this.module;
	}
	
	/**
	 * Assigns the command to a module.
	 *
	 * @param module the module the command will be assigned to
	 */
	void setModule(@Nullable Module module)
	{
		this.module=module;
	}
	
	/**
	 * Returns the argument format for the command.
	 *
	 * @return suitable argument format for this command
	 */
	public abstract String getSyntax();
	
	@Override
	public int compareTo(@NotNull Command o)
	{
		return this.getName().compareToIgnoreCase(o.getName());
	}
}
