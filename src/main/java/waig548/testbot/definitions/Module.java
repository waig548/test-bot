package waig548.testbot.definitions;

import net.dv8tion.jda.core.entities.Guild;

import java.util.Optional;

public enum Module
{
	INFO("info", true, "Information"),
	CONFIG("config", true, "Configuration"),
	UTIL("utility", true, "Utility"),
	FUN("fun", true, "Fun");
	
	private final String translationKey;
	private final boolean enabledByDefault;
	private final String altName;
	
	Module(String translationKey, boolean enabledByDefault, String altName)
	{
		this.translationKey=translationKey;
		this.enabledByDefault=enabledByDefault;
		this.altName=altName;
	}
	
	public static Optional<Module> parse(String input)
	{
		for(Module module: Module.values())
			if(module.name().equalsIgnoreCase(input) || module.altName.equalsIgnoreCase(input))
				return Optional.of(module);
		return Optional.empty();
	}
	
	public String getTranslationKey()
	{
		return this.translationKey;
	}
	
	public boolean isEnabledByDefault()
	{
		return this.enabledByDefault;
	}
	
	public boolean isEnabledIn(Guild guild)
	{
		return this.isEnabledByDefault();    //TODO add guild specific configurations
	}
	
	public String getAltName()
	{
		return this.altName;
	}
	
}
