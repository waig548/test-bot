package waig548.testbot.definitions;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class LogFormatter extends Formatter
{
	@Override
	public String format(LogRecord record)
	{
		return "["+record.getLoggerName()+"] "+record.getLevel()+" "
				+record.getMessage()+"\n";
	}
}
