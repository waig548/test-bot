package waig548.testbot.util.messages;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;

public abstract class Context
{
	public TextChannel textChannel;
	public Guild guild;
	public Member member;
	// --Commented out by Inspection (2020/03/25 23:12):public User user;
	// --Commented out by Inspection (2020/03/25 23:12):private Logger log=LoggerFactory.getLogger(Context.class);
	
}
