package waig548.testbot.util.math;

public class Expression<T extends Number>
{
	protected Expression<T> n1, n2;
	protected Operator o;
	protected T num;
	
	Expression(T num)
	{
		this.num=num;
	}
	
	Expression(Expression<T> n1, Operator o, Expression<T> n2)
	{
		this.n1=n1;
		this.n2=n2;
		this.o=o;
	}
	
	public static <T extends Number> Expression<T> parse(String expr)
	{
		
		int wraps=0, operatorPos=-1;
		Operator operator=Operator.EMPTY;
		for(int i=0; i<expr.length(); i++)
		{
			boolean completed=false;
			if(!Character.isDigit(expr.charAt(i)))
			{
				if(expr.charAt(i)=='(') wraps++;
				if(expr.charAt(i)==')') wraps--;
				if(wraps==0) for(Operator o: Operator.values())
					if(o.isDependencyMet(Expression.class))
						if(expr.indexOf(o.getSyntax(), i)==i)
						{
							if(operator.getPriority()<o.getPriority())
							{
								completed=true;
								break;
							}
							if(operator.getPriority()>o.getPriority())
							{
								operator=o;
								operatorPos=i;
							}
							
						}
				if(completed) break;
			}
		}
		
		if(operatorPos!=-1)
		{
			String left=expr.substring(0, operatorPos);
			if(left.matches("^\\(.+\\)$"))
				left=left.substring(1, left.length()-1);
			String right=expr.substring(operatorPos+operator.getSyntax().length());
			if(right.matches("^\\(.+\\)$"))
				right=right.substring(1, right.length()-1);
			return new Expression<>(parse(left), operator, parse(right));
		}
		else return new Expression<>(NumberParser.parse(expr));
		
	}
	
	public T calculate()
	{
		if(this.num!=null)
		{
			return this.num;
		}
		else
		{
			return this.o.performAction(this.n1, this.n2);
		}
	}
	
	@Override
	public String toString()
	{
		if(this.num!=null)
			return "["+this.num+"]";
		else
			return "["+this.n1.toString()+this.o.getSyntax()+this.n2.toString()+"]";
	}
	
}
