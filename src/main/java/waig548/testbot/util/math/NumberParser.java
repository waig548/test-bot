package waig548.testbot.util.math;

public class NumberParser<T extends Number>
{
	@SuppressWarnings("unchecked")
	public static <T extends Number> T parse(String src)
	{
		if(src==null || src.equals("")) return (T) (new Double(0));
		return (T) (new Double(Double.parseDouble(src)));
	}
}
