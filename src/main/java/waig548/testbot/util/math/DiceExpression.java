package waig548.testbot.util.math;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static waig548.testbot.core.Bot.RANDOM;

public class DiceExpression<T extends Number> extends Expression<T>
{
	private List<T> rolledNumbers=new ArrayList<>();
	private T diceSum;
	
	private DiceExpression(T num)
	{
		super(num);
	}
	
	public DiceExpression(DiceExpression<T> n1, Operator o, DiceExpression<T> n2)
	{
		super(n1, o, n2);
	}
	
	public static <T extends Number> DiceExpression<T> parse(String expr)
	{
		
		int wraps=0, operatorPos=-1;
		Operator operator=Operator.EMPTY;
		for(int i=0; i<expr.length(); i++)
		{
			boolean completed=false;
			if(!Character.isDigit(expr.charAt(i)))
			{
				if(expr.charAt(i)=='(') wraps++;
				if(expr.charAt(i)==')') wraps--;
				for(Operator o: Operator.values())
					if(o.isDependencyMet(DiceExpression.class))
						if(expr.indexOf(o.getSyntax(), i)==i)
						{
							if(operator.getPriority()<o.getPriority()/* && operator!=Operator.SUB && o!=Operator.DICE*/)
							{
								completed=true;
								break;
							}
							if(operator.getPriority()>o.getPriority())
							{
								operator=o;
								operatorPos=i;
							}
							
						}
				if(completed) break;
			}
		}
		
		if(operatorPos!=-1)
		{
			String left=expr.substring(0, operatorPos);
			if(left.matches("^\\(.+\\)$")) left=left.substring(1, left.length()-1);
			String right=expr.substring(operatorPos+operator.getSyntax().length());
			if(right.matches("^\\(.+\\)$")) right=right.substring(1, right.length()-1);
			return new DiceExpression<>(parse(left), operator, parse(right));
		}
		else return new DiceExpression<>(NumberParser.parse(expr));
		
	}
	
	@Override
	public T calculate()
	{
		if(this.o==Operator.DICE)
			return this.roll(this.n1.calculate().intValue(), this.n2.calculate().intValue());
		else return super.calculate();
	}
	
	@SuppressWarnings("unchecked")
	private T roll(int count, int sides)
	{
		if(count<=0) count=1;
		this.diceSum=(T) new Double(0);
		for(int i=0; i<count; i++)
		{
			T rolled=(T) new Double(RANDOM.nextInt(sides)+1);
			this.rolledNumbers.add(rolled);
			this.diceSum=(T) new Double(this.diceSum.doubleValue()+rolled.doubleValue());
		}
		
		return this.diceSum;
	}
	
	@Override
	public String toString()
	{
		if(this.o==Operator.DICE)
		{
			StringBuilder sb=new StringBuilder();
			sb.append("[(");
			Iterator<T> iterator=this.rolledNumbers.iterator();
			while(iterator.hasNext())
			{
				sb.append(iterator.next()).append(iterator.hasNext()? "+": "");
			}
			sb.append("=").append(((this.diceSum)));
			sb.append(")]");
			return sb.toString();
		}
		return super.toString();
	}
}
