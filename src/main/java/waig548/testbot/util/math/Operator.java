package waig548.testbot.util.math;

public enum Operator implements Comparable<Operator>
{
	ADD("addition", "+", OperatorType.ADDITIVE, 0),
	SUB("subtraction", "-", OperatorType.ADDITIVE, 1),
	MUL("multiplication", "*", OperatorType.MULTIPLICATIVE, 0),
	DIV("division", "/", OperatorType.MULTIPLICATIVE, 1),
	MOD("modular", "%", OperatorType.MULTIPLICATIVE, 1),
	DICE("dice", "d", OperatorType.SPECIAL, 0, DiceExpression.class), //LEB("leftBracket", "("),
	//RIB("rightBracket", ")"),
	EMPTY("empty", "empty empty", OperatorType.EMPTY, 0);
	
	private final String name;
	private final String syntax;
	private final OperatorType type;
	private final int subPriority;
	private final Class dependency;
	//private static final HashMap<OperatorType, Set<Operator>> operatorCategories=new HashMap<>();
	
	Operator(String name, String syntax, OperatorType type, int subPriority)
	{
		this(name, syntax, type, subPriority, null);
	}
	
	Operator(String name, String syntax, OperatorType type, int subPriority, Class dependency)
	{
		this.name=name;
		this.syntax=syntax;
		this.type=type;
		this.subPriority=subPriority;
		this.dependency=dependency;
	}
	
	public boolean isDependencyMet(Class clazz)
	{
		if(this.dependency==null) return true;
		if(clazz==null)	return false;
		return this.dependency==clazz;
	}
	
	public int getPriority()
	{
		return this.type.getPriority()*10+this.subPriority;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public OperatorType getType()
	{
		return this.type;
	}
	
	public String getSyntax()
	{
		return this.syntax;
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Number> T performAction(Expression<T> e1, Expression<T> e2)
	{
		if(e1==null || e2==null) return null;
		try
		{
			switch(this.ordinal())
			{
				case 0:
					return (T) new Double(e1.calculate().doubleValue()+e2.calculate().doubleValue());
				case 1:
					return (T) new Double(e1.calculate().doubleValue()-e2.calculate().doubleValue());
				case 2:
					return (T) new Double(e1.calculate().doubleValue()*e2.calculate().doubleValue());
				case 3:
					return (T) new Double(e1.calculate().doubleValue()/e2.calculate().doubleValue());
				case 4:
					if(isInteger(e1) && isInteger(e2))
						return (T) new Integer(e1.calculate().intValue()%e2.calculate().intValue());
					else
						return (T) new Double(Double.NaN);
				case 5:
				default:
					return (T) new Double(0);
			}
		}
		catch(ArithmeticException e)
		{
			return (T) new Double(Double.NaN);
		}
	}
	
	private static <T extends Number> boolean isInteger(Expression<T> e)
	{
		return e.calculate().doubleValue()-(double) e.calculate().intValue()==0d;
	}
	
	enum OperatorType
	{
		SPECIAL(17),
		MULTIPLICATIVE(12),
		ADDITIVE(11),
		EMPTY(99999999);
		
		private final int priority;
		OperatorType(int priority)
		{
			this.priority=priority;
		}
		
		public int getPriority()
		{
			return this.priority;
		}
	}
}
